import { Injectable, NgZone } from "@angular/core";
import {
  CanActivate,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  Router
} from "@angular/router";
import * as firebase from "firebase/app";
import "firebase/auth";
import { Observable } from "rxjs";
import { map } from "rxjs/operators";

@Injectable()
export class RouteGuard implements CanActivate {
  onAuthStateChanged$ = Observable.create(obs => {
    return firebase
      .auth()
      .onAuthStateChanged(
        user => obs.next(user),
        err => obs.error(err),
        () => obs.complete()
      );
  });

  constructor(private router: Router, private ngZone: NgZone) {}

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<boolean> | Promise<boolean> | boolean {
    return this.onAuthStateChanged$.pipe(
      map(val => {
        if (val) {
          return true;
        } else {
          this.ngZone.run(() => this.router.navigate(["/"]));
          return false;
        }
      })
    );
  }
}
