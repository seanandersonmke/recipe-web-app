import { Injectable } from "@angular/core";
import { HttpHeaders, HttpClient } from "@angular/common/http";
import { Observable, Subject, of } from "rxjs";
import { Token } from "./token";
import "firebase/auth";
import * as firebase from "firebase/app";

@Injectable({
  providedIn: "root"
})
export class ConfigService {
  BASEURL: String = "http://localhost:8080";
  httpOptions = {
    headers: new HttpHeaders({
      "Content-Type": "application/json"
    })
  };

  constructor(private http: HttpClient) {}

  getToken(token: String): Observable<Token> {
    if (!token) {
      return of();
    }
    return this.http.post<Token>(
      `${this.BASEURL}/auth`,
      { token: token },
      this.httpOptions
    );
  }
}
