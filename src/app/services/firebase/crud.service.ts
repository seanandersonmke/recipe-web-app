import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { HttpHeaders } from "@angular/common/http";

@Injectable({
  providedIn: "root"
})
export class CrudService {
  baseUrl: String = "http://localhost:8080";
  constructor(private http: HttpClient) {}

  addRecipeName(recipeName) {
    const httpOptions = {
      headers: new HttpHeaders({
        "Content-Type": "application/json"
      })
    };

    let url = `${this.baseUrl}/newrecipe`;
    return this.http.post<any>(url, { name: recipeName }, httpOptions);
  }
}
