import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { AuthComponent } from "./components/auth/auth.component";
import { HomeComponent } from "./components/home/home.component";
import { AddComponent } from "./components/add/add.component";
import { ViewComponent } from "./components/view/view.component";
import { ConfigService } from "./services/firebase/config.service";
import { RouteGuard } from "./services/auth/RouteGuard";

const routes: Routes = [
  { path: "home", component: HomeComponent, canActivate: [RouteGuard] },
  { path: "add", component: AddComponent, canActivate: [RouteGuard] },
  { path: "view", component: ViewComponent, canActivate: [RouteGuard] },
  { path: "", component: AuthComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: [RouteGuard]
})
export class AppRoutingModule {
  constructor() {}
}
