import { Component, OnInit } from "@angular/core";
import { ConfigService } from "../../services/firebase/config.service";
import * as firebase from "firebase/app";

@Component({
  selector: "app-nav",
  templateUrl: "./nav.component.html",
  styleUrls: ["./nav.component.scss"]
})
export class NavComponent implements OnInit {
  constructor(private configService: ConfigService) {}

  logout() {
    firebase
      .auth()
      .signOut()
      .then(function() {
        console.log("logged out");
        document.location.replace("/");
      })
      .catch(function(error) {
        console.log(error);
      });
  }

  ngOnInit() {}
}
