import { Component, OnInit } from "@angular/core";
// import { library, icon } from "@fortawesome/fontawesome-svg-core";
// import { faIgloo } from "@fortawesome/free-solid-svg-icons";
import { CrudService } from "../../services/firebase/crud.service";

// library.add(faIgloo);

@Component({
  selector: "app-add",
  templateUrl: "./add.component.html",
  styleUrls: ["./add.component.scss"]
})
export class AddComponent implements OnInit {
  formData: any = {};
  itemsList: Array<String> = [];
  showModal: boolean = false;
  nameModal: boolean = true;
  recipeName: String;

  // igloo = icon.icon({ prefix: "fas", iconName: "igloo" });

  constructor(private crudService: CrudService) {}

  addItem() {
    this.itemsList.push(this.formData);
    this.formData = {};
  }

  deleteItem() {}

  editItem(data: Object) {
    if (this.formData != data) {
      this.formData = data;
    }

    this.showModal = true;
  }

  addRecipeName() {
    this.crudService
      .addRecipeName(this.recipeName)
      .subscribe(data => console.log("hit subscribe"));
  }

  refreshItem() {
    this.formData = {};
    this.openModal();
  }

  openModal() {
    this.showModal = true;
  }

  ngOnInit() {}
}
