import { Component, OnInit } from "@angular/core";
import { ConfigService } from "../../services/firebase/config.service";
import { Router } from "@angular/router";
import * as firebase from "firebase/app";
import * as firebaseui from "firebaseui";
import "firebase/auth";

@Component({
  selector: "app-auth",
  templateUrl: "./auth.component.html",
  styleUrls: ["./auth.component.scss"]
})
export class AuthComponent implements OnInit {
  baseUrl: String = "http://localhost:8080";

  constructor(private configService: ConfigService, private router: Router) {}

  configUi() {
    let ui = new firebaseui.auth.AuthUI(firebase.auth());
    ui.start("#firebaseui-auth-container", {
      signInOptions: [
        firebase.auth.EmailAuthProvider.PROVIDER_ID,
        firebase.auth.GoogleAuthProvider.PROVIDER_ID
      ],
      signInSuccessUrl: "/home"
    });
  }

  checkLogin() {
    firebase.auth().onAuthStateChanged(user => {
      if (user) {
        this.router.navigate(["/home"]);
      } else {
        this.configUi();
      }
    });
  }

  ngOnInit() {
    this.checkLogin();
  }
}
