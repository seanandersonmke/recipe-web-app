import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { AppRoutingModule } from "./app-routing.module";
import { HttpClientModule } from "@angular/common/http";
import { MainComponent } from "./components/main/main.component";
import { HomeComponent } from "./components/home/home.component";
import { AuthComponent } from "./components/auth/auth.component";
import { AddComponent } from "./components/add/add.component";
import { ViewComponent } from "./components/view/view.component";
import { NavComponent } from "./components/nav/nav.component";
import * as firebase from "firebase";
import { FirebaseConfig } from "src/environments/FirebaseConfig.js";

firebase.initializeApp(FirebaseConfig);

@NgModule({
  declarations: [
    MainComponent,
    HomeComponent,
    AuthComponent,
    AddComponent,
    ViewComponent,
    NavComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [MainComponent]
})
export class AppModule {}
